/*
 * Author: Eduardo Miravalls
 * Date: 2015-05-06
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* strcmp() */
#include <stdbool.h>
#include <assert.h>

struct node {
	struct node *next;
	int key;
};

/*
 * Compile with -DDPRINTS to see debugging prints.
 */
#if DPRINTS
#define dprintf(fmt, ...) \
	printf("(%s): " fmt, __func__, ##__VA_ARGS__)

#else
#define dprintf(fmt, ...) (void)0
#endif

struct node *removeNthFromEnd(struct node **head, size_t n);

/*
 * Auxiliary functions:
 */

#define N 10

/* create a linked list of length len */
int create_chain(struct node **head, size_t len, struct node *pool)
{
	size_t i;
	struct node **ptr;

	if (len > N) {
		return -1;
	}

	if (len == 0) {
		*head = NULL;
		return 0;
	}

	for (i = 0, ptr = head; i < len; ptr = &(*ptr)->next) {
		*ptr = &pool[i];
		(*ptr)->key = i;
		i++;
		(*ptr)->next = &pool[i];
	}

	*ptr = NULL;
	return 0;
}

/* returns list's length */
size_t chain_length(struct node *head)
{
	size_t len;

	for (len = 0; head != NULL; len++, head = head->next)
		;

	return len;
}

/* verify that only the expected key has been deleted */
void verify_chain(struct node *head, size_t len, int missing)
{
	int i;

	dprintf("head=%p, expected number of checks=%zu, missing=%d\n", head, len, missing);

	for (i = 0; head != NULL && i < missing; i++, head = head->next) {
		dprintf("head->key=%d\n", head->key);
		len--;
		assert(head->key == i);
	}

	if (i == missing) {
		i++;
	}

	for (; head != NULL; i++, head = head->next) {
		dprintf("head->key=%d\n", head->key);
		len--;
		assert(head->key == i);
	}

	assert(len == 0);
}

/*
 * Tests of previous auxiliary functions
 */

/* test creation of a list of length 0 */
void create_chain_test0(struct node *pool)
{
	struct node *head;

	create_chain(&head, 0, pool);
	assert(head == NULL);
	dprintf("chain's lenght=%zu\n", chain_length(head));
	assert(chain_length(head) == 0);
	verify_chain(head, 0, -1);
}

/* test creation of a list of length 1 */
void create_chain_test1(struct node *pool)
{
	struct node *head;

	create_chain(&head, 1, pool);
	assert(head != NULL);
	dprintf("chain's lenght=%zu\n", chain_length(head));
	assert(chain_length(head) == 1);
	verify_chain(head, 1, -1);
}

/* test creation of a list of length 3 */
void create_chain_test3(struct node *pool)
{
	struct node *head;

	create_chain(&head, 3, pool);
	assert(head != NULL);
	dprintf("chain's lenght=%zu\n", chain_length(head));
	assert(chain_length(head) == 3);
	verify_chain(head, 3, -1);
}

/*
 * removeNthFromEnd test cases
 */
void removeNthFromEnd_test(size_t len, int n, bool should_succeed, struct node *pool)
{
	struct node *head;

	dprintf("len=%zu, n=%d, should_succeed=%s\n",
	        len, n, should_succeed ? "true" : "false");

	create_chain(&head, len, pool);
	assert(chain_length(head) == len);

	if (should_succeed) {
		assert(removeNthFromEnd(&head, n) != NULL);
		assert(chain_length(head) == len - 1);
		verify_chain(head, len - 1, len - 1 - n);

	} else {
		assert(removeNthFromEnd(&head, n) == NULL);
		assert(chain_length(head) == len);
		verify_chain(head, len, len);
	}
}

/* Test cases for removeNthFromEnd with a chain of length 1. */
void removeNthFromEnd_len1(struct node *pool)
{
	removeNthFromEnd_test(1, 0, true, pool);
	removeNthFromEnd_test(1, 1, false, pool);
	removeNthFromEnd_test(1, 6, false, pool);
}

/* Test cases for removeNthFromEnd with a chain of length 3. */
void removeNthFromEnd_len3(struct node *pool)
{
	removeNthFromEnd_test(3, 0, true, pool);
	removeNthFromEnd_test(3, 1, true, pool);
	removeNthFromEnd_test(3, 2, true, pool);
	removeNthFromEnd_test(3, 3, false, pool);
	removeNthFromEnd_test(3, 6, false, pool);
}

int main(int argc, const char *argv[])
{
	/*
	   instead of doing a bunch of allocations,
	   I'll use a pool of N nodes.
	 */
	struct node *pool;

	if ((pool = calloc(N, sizeof(*pool))) == NULL) {
		perror("calloc");
		return -1;
	}

	if (argc > 1 && strcmp(*argv, "--test-create_chain")) {
		create_chain_test0(pool);
		create_chain_test1(pool);
		create_chain_test3(pool);

	} else {
		removeNthFromEnd_len1(pool);
		removeNthFromEnd_len3(pool);
	}

	free(pool);
	return 0;
}
