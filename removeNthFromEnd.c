/*
 * Author: Eduardo Miravalls
 * Date: 2015-05-06
 *
 * Given a Singly Linked List, delete nth node from end.
 * Example:
 * Assume l=0->1->2->3->4->5
 *
 * Delete 0 should return l=0->1->2->3->4
 * Delete 1 should return l=0->1->2->3->5
 * Delete 2 should return l=0->1->2->4->5
 * Delete 3 should return l=0->1->3->4->5
 * Delete 4 should return l=0->2->3->4->5
 * Delete 5 should return l=1->2->3->4->5
 * Delete (anything else) => ERROR. l=0->1->2->3->4->5
 */

#include <stdlib.h>

struct node {
	struct node *next;
	int key;
};

 /**
  * @brief remove n-th node from end of list.
  * @details counting starts at 0, so last node would be n=0; the node
  * before, n=1; and so on.
  * In order to succeed, n has to be between 0 and list's length - 1.
  *
  * @param head pointer to head.
  * @param n position of node to be removed, counted from last to first.
  * @param f node's destructor. Can be NULL.
  *
  * @return On success, pointer to the removed node.
  * @return On failure (n >= than list's length), NULL.
  */
struct node *removeNthFromEnd(struct node **head, size_t n)
{
	struct node **target; /* node to be removed */
	struct node *ret;
	size_t i;

	n++;
	target = head;

	for (i = 0; i < n && *head != NULL; i++) {
		head = &(*head)->next;
	}

	/*
	   the following if could be removed
	   if n is assumed to be < list's length
	 */
	if (i != n) {
		return NULL; /* Error: n >= list's length */
	}

	while (*head != NULL) {
		head = &(*head)->next;
		target = &(*target)->next;
	}

	ret = *target;
	*target = (*target)->next;
	return ret;
}
