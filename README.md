My solution to the problem of deleting the nth node from the end of a linked list.

I came across this problem reading this post:
http://codereview.stackexchange.com/questions/63247/remove-nth-node-from-end-of-linked-list